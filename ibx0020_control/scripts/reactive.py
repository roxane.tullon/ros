#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image

import tf   # for a function to transform quaternion into euler
import math # for sqrt and atan2 functions
import sys  # for reading the arguments
import colorsys

#global variable storing the command published to cmd_vel
cmd = Twist()

# global variables to store the last received odometry, laser, and image readings
lastOdomReading = None
lastLaserReading = None
lastImageReading = None
imageProcessedData = None

# callback function to process data from subscribed Odometry topic
def odometryReceived(data):
    global lastOdomReading
    lastOdomReading = data

# callback function to process data from subscribed LaserScan topic
def laserReceived(data):
    global lastLaserReading
    lastLaserReading = data

# callback function to process data from subscribed Image topic
def imageReceived(data):
    global lastImageReading
    global imageProcessedData
    lastImageReading = data.data
    imageProcessedData = []
    degrees = 1.0472 # rad, ~60 degrees, for HSV conversion
    for x in range(0, 120):
        #normalizing the colors
        R = ord(lastImageReading[x*4]) / 255.0
        G = ord(lastImageReading[x*4 + 1]) / 255.0
        B = ord(lastImageReading[x*4 + 2]) / 255.0
        # converting to HSV
        if R > G and R > B:
            # calculating hue
            H = degrees*((G - B) / R - min(G, B))
            if H < 0: # correcting the negative angle
                H += 2 * math.pi
            elif H > 2 * math.pi: # correcting angle over 360 degrees
                H -= 2 * math.pi
            # calculating saturation
            S = (R - min(G, B)) / R

            if H < 0.05 and S > 0.8:
                imageProcessedData.append(1)
        else:
            imageProcessedData.append(0)

# 
def GO_NORTH_EAST(th):
    global cmd
    goal_th = 0.785398  # radians, ~45 degrees
    dth = goal_th - th
    cmd.linear.x = 1
    cmd.angular.z = normalizeAngle(dth) # rotational speed

# the obstacle avoidance logic
def OBSTACLE_AVOIDANCE(closestObstacleDistance):
    global cmd

    goal_distance = 0.8 # meters, the desired distance to the closest obstacle
    ddistance = closestObstacleDistance - goal_distance    # difference between the desired and the actual middle distance

    v = max([0, ddistance])

    leftObstacleDistance = min([lastLaserReading.ranges[7], lastLaserReading.ranges[8], lastLaserReading.ranges[9], \
        lastLaserReading.ranges[10], lastLaserReading.ranges[11], lastLaserReading.ranges[12]])

    rightObstacleDistance = min([lastLaserReading.ranges[0], lastLaserReading.ranges[1], lastLaserReading.ranges[2], \
        lastLaserReading.ranges[3], lastLaserReading.ranges[4], lastLaserReading.ranges[5]])

    w = leftObstacleDistance - rightObstacleDistance

    cmd.linear.x = v
    cmd.angular.z = w

#
def STOP_AT_RED_LIGHT():
    centerOfImage = int(119 / 2) # center pixel of the camera image

    # locating the red object
    # finding the first occurence of '1' in the array - the beginning of the object
    redObjectBeginning = imageProcessedData.index(1)
    # finding the last occurencr of '1' in the array - the end of the object
    redObjectEnd =  len(imageProcessedData) - 1 - imageProcessedData[::-1].index(1)
    # finding the middle (center) of the object
    redObjectMiddle = average([redObjectBeginning, redObjectEnd])

    cmd.linear.x = 0 # stopping the robot because the red object is visible
    cmd.angular.z = 0.1 * (centerOfImage - redObjectMiddle) # rotating the center of camera towards the center of object

# utility function to normalyze angles (-pi <= a <= pi)
def normalizeAngle(a):
    while a < -math.pi:
        a += 2.0*math.pi
    while a > +math.pi:
        a -= 2.0*math.pi
    return a

# utility function to find the mean value (average)
def average(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

# main function of the node
def reactive():
    global lastOdomReading
    global lastLaserReading
    global lastImageReading
    global cmd

    # subscribing to odometry and announcing published topics
    rospy.Subscriber("base_pose_ground_truth", Odometry, odometryReceived)
    rospy.Subscriber("base_scan", LaserScan, laserReceived)
    rospy.Subscriber("image", Image, imageReceived)
    cmd_pub = rospy.Publisher('cmd_vel', Twist)

    r = rospy.Rate(10)  # an object to maintain specific frequency of a control loop - 10hz

    while not rospy.is_shutdown():
        if lastOdomReading is None:  # we cannot issue any commands until we have our position
            print 'waiting for lastOdomReading to become available'
            r.sleep()
            continue

        # current robot orientation
        pose = lastOdomReading.pose.pose  # robots current pose (position and orientation)
        # euler_from_quaternion returns array of [pitch, roll, yaw], we need only yaw (rotation around Z axis)
        th = tf.transformations.euler_from_quaternion([pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w])[2]

        # calculating the distance to the closest obstacle, which is the minimum value from the distances of the seven middle lasers
        closestObstacleDistance = min(lastLaserReading.ranges)

        if 1 in imageProcessedData:
            STOP_AT_RED_LIGHT()
        elif closestObstacleDistance < 1.4:
            OBSTACLE_AVOIDANCE(closestObstacleDistance)
        else:
            GO_NORTH_EAST(th)
        
        cmd_pub.publish(cmd)

        # sleeping so, that the loop won't run faster than r's frequency
        r.sleep()
        # end of loop
    # end of function

# entry point of the executable
# calling the main node function of the node only if this .py file is executed directly, not imported
if __name__ == '__main__':
    rospy.init_node('reactive')
    reactive()
