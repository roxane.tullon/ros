#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image

import tf   # for a function to transform quaternion into euler
import math # for sqrt and atan2 functions
import sys  # for reading the arguments

#global variable storing the name of the robot node ("robot_#")
robotNodeName = ""

#global variable storing the command published to cmd_vel
cmd = Twist()

# global variables to store the last received odometry, laser, and image readings
lastOdomReading = None
lastLaserReading = None
lastImageReading = None

# callback function to process data from subscribed Odometry topic
def odometryReceived(data):
    global lastOdomReading
    lastOdomReading = data

# callback function to process data from subscribed LaserScan topic
def laserReceived(data):
    global lastLaserReading
    lastLaserReading = data

# callback function to process data from subscribed Image topic
def imageReceived(data):
    global lastImageReading
    lastImageReading = data

# 
def GO_NORTH_EAST(th):
    global cmd
    goal_th = 0.785398  # radians, ~45 degrees
    dth = goal_th - th
    cmd.linear.x = 1
    cmd.angular.z = normalizeAngle(dth) # rotational speed

# 
def OBSTACLE_AVOIDANCE(closestObstacleDistance):
    global cmd

    goal_distance = 1.2 # meters, the desired distance to the closest obstacle
    ddistance = goal_distance - lastLaserReading.ranges[6]    # difference between the desired and the actual middle distance
    if ddistance <= 0:
        v = 0
    else:
        v = ddistance   # velocity, the distance difference acts as a coefficient

    w = (lastLaserReading.ranges[12] - lastLaserReading.ranges[0]) \
        + (lastLaserReading.ranges[11] - lastLaserReading.ranges[1]) \
        + (lastLaserReading.ranges[10] - lastLaserReading.ranges[2]) \
        + (lastLaserReading.ranges[9] - lastLaserReading.ranges[3]) \
        + (lastLaserReading.ranges[8] - lastLaserReading.ranges[4]) \
        + (lastLaserReading.ranges[7] - lastLaserReading.ranges[5])

    cmd.linear.x = v
    cmd.angular.z = w

# utility function to normalyze angles (-pi <= a <= pi)
def normalizeAngle(a):
    while a < -math.pi:
        a += 2.0*math.pi
    while a > +math.pi:
        a -= 2.0*math.pi
    return a

# utility function to find the mean value (average)
def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

# main function of the node
def control():
    global lastOdomReading
    global lastLaserReading
    global lastImageReading
    global cmd

    # subscribing to odometry and announcing published topics
    rospy.Subscriber(robotNodeName + "/base_pose_ground_truth", Odometry, odometryReceived)
    rospy.Subscriber(robotNodeName + "/base_scan", LaserScan, laserReceived)
    rospy.Subscriber(robotNodeName + "/image", Image, imageReceived)
    cmd_pub = rospy.Publisher(robotNodeName + '/cmd_vel', Twist)

    r = rospy.Rate(10)  # an object to maintain specific frequency of a control loop - 10hz

    while not rospy.is_shutdown():
        if lastOdomReading is None:  # we cannot issue any commands until we have our position
            print 'waiting for lastOdomReading to become available'
            r.sleep()
            continue

        # current robot orientation
        pose = lastOdomReading.pose.pose  # robots current pose (position and orientation)
        # euler_from_quaternion returns array of [pitch, roll, yaw], we need only yaw (rotation around Z axis)
        th = tf.transformations.euler_from_quaternion([pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w])[2]

        # determining the distance to the closest obstacle in front of the robot
        closestObstacleDistance = min([lastLaserReading.ranges[4], lastLaserReading.ranges[5], \
            lastLaserReading.ranges[6], lastLaserReading.ranges[7], lastLaserReading.ranges[8]])

        if closestObstacleDistance < 1.4:
            OBSTACLE_AVOIDANCE(closestObstacleDistance)
        else:
            GO_NORTH_EAST(th)

        # control equations (slide 25)
        # p = math.sqrt(dx*dx + dy*dy)
        # a = normalizeAngle(-th + math.atan2(dy, dx))
        # b = normalizeAngle(dth - a) # corrected this one
        # control equations (slide 26)
        # v = p  # translational speed in m/s
        # w = normalizeAngle(a + b)  # rotational speed in rad/s
        # setting command fields
        # cmd.linear.x = v
        # cmd.angular.z = w
        # publishing command to a robot
        cmd_pub.publish(cmd)

        # sleeping so, that the loop won't run faster than r's frequency
        r.sleep()
        # end of loop
    # end of function

# entry point of the executable
# calling the main node function of the node only if this .py file is executed directly, not imported
if __name__ == '__main__':
    if len(sys.argv) > 1:
        robotNodeName = sys.argv[1]
    rospy.init_node('control')
    control()
